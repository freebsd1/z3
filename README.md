# z3

**Backup/Restore ZFS Snapshots to S3**

Pre-compiled z3 bin files are placed at /bin directory. Copy all the binary files and place it in /usr/local/bin directory.

Site-packages directory contains the required python module files for this tool. Copy all the directories and place it in /usr/local/lib/python2.7/site-packages/ .

Sample.conf is the configuration file for this tool. Rename this file as z3.conf and place it under /etc directory.

For detailed instruction on how to work on this tool, please refer this blog https://www.assistanz.com/zfs-snapshots-to-s3-bucket-backup-restore-using-z3-tool 
